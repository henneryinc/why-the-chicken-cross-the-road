﻿using UnityEngine;
using System.Collections;

public class MoveLine : MonoBehaviour {

    public float speedX;
    public float speedY;
    public float speedZ;

    private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rigidbody.velocity = new Vector3(speedX, speedY, speedZ);
	}
}
