﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameController : MonoBehaviour {

    public GameObject terrainLine;
    public GameObject boundary;
	public GameObject currencyObject;

    
    public GameObject Rock;
    public GameObject BuracoEsquerda;
    public GameObject BuracoCentro;
    public GameObject BuracoDireita;
    public GameObject ObestaculoDinamico;
    bool bSpwanObj = false;

    Text currency;
    int currencySpawnTime = 0; //seconds
    int currencySpawnProbability = 10;// (1/x) probability of spawn 

    Vector3 spawnPosition;
    DateTime startTime;
    System.Random rnd;

    // Use this for initialization
    void Start () {
        currency = GameObject.Find("/UI/Currency").GetComponent<Text>();
        spawnPosition = new Vector3(boundary.transform.position.x, boundary.transform.position.y, boundary.transform.position.z - (boundary.transform.localScale.z / 2));
        startTime = DateTime.Now;
        rnd = new System.Random();
        Vector3 currentSpawnPosition;
        int i = 0;
		while( i < boundary.transform.localScale.z+1)
        {
            currentSpawnPosition = new Vector3(spawnPosition.x, spawnPosition.y, spawnPosition.z +i);
            Debug.Log(spawnPosition.z + i);
            Instantiate(terrainLine, currentSpawnPosition, Quaternion.identity);
            createNewCurrency(i);
            i += 2;
        }
    }

	void Update()
	{

	}

	/*void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag = "FloorLine") {
			Physics.IgnoreCollision (collision.collider, GetComponent<Collider>());
		}
	}*/

    public void createNewFloorLine()
    {
        SpawnObjetos();//Instantiate(terrainLine, spawnPosition, Quaternion.identity);
    }

    public void createNewCurrency(int i)
    {
        if((DateTime.Now.Ticks - startTime.Ticks) > currencySpawnTime * 10000000 && rnd.Next(currencySpawnProbability) == 0)
        {
            startTime = DateTime.Now;
            switch (rnd.Next(3))
            {
                case 0:
                    Instantiate(currencyObject, new Vector3(-1, 0.75f, spawnPosition.z + i), Quaternion.identity);
                    break;
                case 1:
                    Instantiate(currencyObject, new Vector3(0, 0.75f, spawnPosition.z + i), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(currencyObject, new Vector3(1, 0.75f, spawnPosition.z + i), Quaternion.identity);
                    break;
                default:
                    break;
            }
        }

    }

    public void setCurrency(int currency)
    {
        this.currency.text = currency.ToString();
    }

    public void addCurrency(int currency)
    {
        this.currency.text = (Convert.ToInt32(this.currency.text) + currency).ToString();
    }

    public int getCurrency()
    {
        return Convert.ToInt32(currency.text);
    }

    public void SpawnObjetos()
    {
        if (!bSpwanObj)
        {
            bSpwanObj = true;
            Vector3 spawnPositionObject;

            int nOpcoes = rnd.Next(4) + 1;
            int nLocal = rnd.Next(3)+1;
            Debug.Log("SpawnObjetos");
            Debug.Log(spawnPosition.z);
            switch (nOpcoes)
            {
                case 1://Gera tipoq
                    Instantiate(terrainLine, spawnPosition, Quaternion.identity); //estrada Normal
                    switch (nLocal)
                    {
                        case 1://esquerda
                            spawnPositionObject = new Vector3(spawnPosition.x - 1, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(Rock, spawnPositionObject, Quaternion.identity);
                            break;
                        case 2://centro
                            spawnPositionObject = new Vector3(spawnPosition.x, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(Rock, spawnPositionObject, Quaternion.identity);
                            break;
                        case 3://direita
                            spawnPositionObject = new Vector3(spawnPosition.x + 1, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(Rock, spawnPositionObject, Quaternion.identity);
                            break;
                        default://centro
                            spawnPositionObject = new Vector3(spawnPosition.x, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(Rock, spawnPositionObject, Quaternion.identity);
                            break;
                    }
                    
                    break;
                case 2://gera Tipo2
                    Instantiate(terrainLine, spawnPosition, Quaternion.identity); //estrada Normal
                    switch (nLocal)
                    {
                        case 1://esquerda
                            spawnPositionObject = new Vector3(spawnPosition.x - 1, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(ObestaculoDinamico, spawnPositionObject, Quaternion.identity);
                            break;
                        case 2://centro
                            spawnPositionObject = new Vector3(spawnPosition.x, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(ObestaculoDinamico, spawnPositionObject, Quaternion.identity);
                            break;
                        case 3://direita
                            spawnPositionObject = new Vector3(spawnPosition.x + 1, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(ObestaculoDinamico, spawnPositionObject, Quaternion.identity);
                            break;
                        default://direita
                            spawnPositionObject = new Vector3(spawnPosition.x + 1, spawnPosition.y + 0.75f, spawnPosition.z);
                            Instantiate(ObestaculoDinamico, spawnPositionObject, Quaternion.identity);
                            break;
                    }
                    
                    break;
                case 3://gera tipo 3 //BURACO
                    switch (nLocal)
                    {
                        case 1://esquerda
                            Instantiate(BuracoEsquerda, spawnPosition, Quaternion.identity);
                            break;
                        case 2://centro
                            Instantiate(BuracoCentro, spawnPosition, Quaternion.identity);
                            break;
                        case 3://direita
                            Instantiate(BuracoDireita, spawnPosition, Quaternion.identity);
                            break;
                        default://direita
                            Instantiate(BuracoDireita, spawnPosition, Quaternion.identity);
                            break;
                    }
                    
                    break;
                case 4:// não gera nada
                    Instantiate(terrainLine, spawnPosition, Quaternion.identity); //estrada Normal
                    break;
                default://não gera nada
                    Instantiate(terrainLine, spawnPosition, Quaternion.identity); //estrada Normal
                    break;
            }
        }
        else
        {
            Instantiate(terrainLine, spawnPosition, Quaternion.identity);//estrada Normal
            bSpwanObj = false;
        }


    }
}
