﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour {

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "FloorLine")
        {
            GameController gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            gameController.createNewFloorLine();
            gameController.createNewCurrency(0);
        }

        Destroy(other.gameObject);
    }
}
