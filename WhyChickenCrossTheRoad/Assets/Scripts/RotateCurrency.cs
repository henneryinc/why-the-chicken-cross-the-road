﻿using UnityEngine;
using System.Collections;

public class RotateCurrency : MonoBehaviour {

	public float speedOfRotation;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up * speedOfRotation * Time.deltaTime);
	}
}
