﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


	public float PlataformSpeed;
	public float sideForce;
	public float jumpForce;
	private Rigidbody myRigidbody;

	private Vector2 touchPoisition;
	private float swipeResistance = 100f;

    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();

        myRigidbody = GetComponent<Rigidbody> ();

        animator.SetBool("IsWalking", true);
    }

	// Update is called once per frame
	void Update () {

        if(transform.position.y > 0.51)
        {
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsJumping", true);
        }
        else
        {
            animator.SetBool("IsJumping", false);
            animator.SetBool("IsWalking", true);
        }


        myRigidbody.velocity = new Vector3 (myRigidbody.velocity.x,myRigidbody.velocity.y,PlataformSpeed);

        
        if (Input.GetMouseButtonDown (0) || Input.GetMouseButtonDown (1)) {

			touchPoisition = Input.mousePosition;
		}


		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)) {

			float swipeForceX = touchPoisition.x - Input.mousePosition.x;
			float swipeForceY = touchPoisition.y - Input.mousePosition.y;
			if (Mathf.Abs (swipeForceX) > swipeResistance) 
			{
				if (swipeForceX < 0  &&  swipeForceY >-80){//Right
					if (transform.position.x  > -0.7) {//para nao ir para a berma
						if (transform.position.y < 0.6) {//para nao mudar de posição quando salta
						this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x +-sideForce,0.5f,0);
						}
					}
				}
				if (swipeForceX > 0 && swipeForceY >-80 ) {//Left
					if (transform.position.x < 0.7) {//para nao ir para a berma
						if (transform.position.y < 0.6) {//para nao mudar de posição quando salta
						this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x +sideForce,0.5f,0);
						}
					}
				}


			}
			if (Mathf.Abs (swipeForceY) > swipeResistance) 
			{

				if (swipeForceY > 0 && swipeForceX <0) {//down
					//myRigidbody.velocity = new Vector3 (myRigidbody.velocity.x, jumpForce, myRigidbody.velocity.z);
				}
				if (swipeForceY < 0) {//up
					if (transform.position.y < 0.6) {//inicialmente ele começa no Y em 0.5, qunado saltar e passar de 0.6 no Y não pode saltar outra vez
						myRigidbody.velocity = new Vector3 (myRigidbody.velocity.x, jumpForce, myRigidbody.velocity.z);
					}
				}

			}
		}

	}

}
