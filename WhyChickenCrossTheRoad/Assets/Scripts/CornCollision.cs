﻿using UnityEngine;
using System.Collections;

public class CornCollision : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player") {
			GameController gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
			gameController.addCurrency(1);
			Destroy(gameObject);
		}
	}

}
